#include <unistd.h>
#include "GPXParser.h"

#define EARTH_RADIUS 6371000

/******************************* String helper functions *******************************/
//Appends src string to dest, allocates more memory if necessary.
void dynamicAppend(char ** dest, const char * src, int * memsize) {
    if (strlen(*dest) + strlen(src) + 2 > *memsize) {
        *dest = realloc(*dest, strlen(src) + *memsize + 50);
        *memsize += strlen(src) + 50;
    }
    strcat(*dest, src);
}

//Appends src double to dest, allocates more memory if necessary.
void dynamicAppendDouble(char ** dest, double src, int * memsize) {
    char srcstr[75];
    sprintf(srcstr, "%f", src);
    if (strlen(*dest) + strlen(srcstr) + 1 > *memsize) {
        *dest = realloc(*dest, strlen(srcstr) + *memsize + 50);
        *memsize += strlen(srcstr) + 50;
    }
    strcat(*dest, srcstr);
}
/***************************************************************************************/


/******************************** List helper functions ********************************/
//Frees a GPXData struct.
void deleteGpxData(void * data) {
    if (data == NULL) {
        return;
    }
    free((GPXData *)(data));
}

//Returns a string representation of a GPXData struct.
char * gpxDataToString(void * data) {
    if (data == NULL) {
        return NULL;
    }
    GPXData * gpxData = (GPXData *)(data);
    int mallocSize = 100;
    char * datastr = malloc(mallocSize);
    datastr[0] = '\0';
    dynamicAppend(&datastr, "  GPXData: ", &mallocSize);
    dynamicAppend(&datastr, gpxData->name, &mallocSize);
    dynamicAppend(&datastr, ", ", &mallocSize);
    dynamicAppend(&datastr, gpxData->value, &mallocSize);
    dynamicAppend(&datastr, "\n", &mallocSize);
    return datastr;
}

//Compares two GPXData structs.
int compareGpxData(const void *first, const void *second) {
    if (first == NULL || second == NULL) {
        return -1;
    }
    GPXData * gpx1 = (GPXData *)(first);
    GPXData * gpx2 = (GPXData *)(second);
    if (strcmp(gpx1->name, gpx2->name) == 0) {
        return 1;
    }
    return 0;
}

//Frees a waypoint.
void deleteWaypoint(void * data) {
    if (data == NULL) {
        return;
    }
    Waypoint * wpt = (Waypoint *)(data);
    free(wpt->name);
    freeList(wpt->otherData);
    free(wpt);
}

//Returns a string representation of a waypoint.
char * waypointToString(void * data) {
    if (data == NULL) {
        return NULL;
    }
    Waypoint * wpt = (Waypoint *)(data);
    int mallocSize = 100;
    char * wptstr = malloc(mallocSize);
    wptstr[0] = '\0';
    dynamicAppend(&wptstr, "Waypoint: ", &mallocSize);
    dynamicAppend(&wptstr, wpt->name, &mallocSize);
    dynamicAppend(&wptstr, " (", &mallocSize);
    dynamicAppendDouble(&wptstr, wpt->latitude, &mallocSize);
    dynamicAppend(&wptstr, ", ", &mallocSize);
    dynamicAppendDouble(&wptstr, wpt->longitude, &mallocSize);
    dynamicAppend(&wptstr, ")\n", &mallocSize);

    char * datastr = toString(wpt->otherData);
    dynamicAppend(&wptstr, datastr, &mallocSize);
    free(datastr);
    return wptstr;
}

//Returns 1 if waypoints have the same name.
int compareWaypoints(const void * first, const void * second) {
    if (first == NULL || second == NULL) {
        return -1;
    }
    Waypoint * wpt1 = (Waypoint *)(first);
    Waypoint * wpt2 = (Waypoint *)(second);
    if (strcmp(wpt1->name, wpt2->name) == 0) {
        return 1;
    }
    return 0;
}

//Frees a route.
void deleteRoute(void * data) {
    if (data == NULL) {
        return;
    }
    Route * rte = (Route *)(data);
    freeList(rte->waypoints);
    freeList(rte->otherData);
    free(rte->name);
    free(rte);
}

//Returns a string representation of a route.
char * routeToString(void * data) {
    if (data == NULL) {
        return NULL;
    }
    Route * rte = (Route *)(data);
    int mallocSize = 100;
    char * rtestr = malloc(mallocSize);
    rtestr[0] = '\0';
    dynamicAppend(&rtestr, "\n~~~~", &mallocSize);
    dynamicAppend(&rtestr, rte->name, &mallocSize);
    dynamicAppend(&rtestr, "~~~~\n", &mallocSize);
    char * gpxstr = toString(rte->otherData);
    dynamicAppend(&rtestr, gpxstr, &mallocSize);
    free(gpxstr);
    char * wptstr = toString(rte->waypoints);
    dynamicAppend(&rtestr, wptstr, &mallocSize);
    free(wptstr);
    return rtestr;
}

//Returns 1 if routes have the same name.
int compareRoutes(const void * first, const void * second) {
    if (first == NULL || second == NULL) {
        return -1;
    }
    Route * rte1 = (Route *)(first);
    Route * rte2 = (Route *)(second);
    if (strcmp(rte1->name, rte2->name) == 0) {
        return 1;
    }
    return 0;
}

//Frees a track segment.
void deleteTrackSegment(void * data) {
    if (data == NULL) {
        return;
    }
    TrackSegment * seg = (TrackSegment *)(data);
    freeList(seg->waypoints);
    free(seg);
}

//Returns a string representation of a track segment.
char * trackSegmentToString(void * data) {
    if (data == NULL) {
        return NULL;
    }
    TrackSegment * seg = (TrackSegment *)(data);
    int mallocSize = 100;
    char * segstr = malloc(mallocSize);
    segstr[0] = '\0';
    dynamicAppend(&segstr, "---Segment Start---\n", &mallocSize);
    char * wptstr = toString(seg->waypoints);
    dynamicAppend(&segstr, wptstr, &mallocSize);
    free(wptstr);
    dynamicAppend(&segstr, "----Segment End----\n", &mallocSize);
    return segstr;
}

//Returns 1 if track segments have the same number of waypoints.
int compareTrackSegments(const void * first, const void * second) {
    if (first == NULL || second == NULL) {
        return -1;
    }
    TrackSegment * seg1 = (TrackSegment *)(first);
    TrackSegment * seg2 = (TrackSegment *)(second);
    if (getLength(seg1->waypoints) == getLength(seg2->waypoints)) {
        return 1;
    }
    return 0;
}

//Frees a track.
void deleteTrack(void * data) {
    if (data == NULL) {
        return;
    }
    Track * trk = (Track *)(data);
    freeList(trk->segments);
    freeList(trk->otherData);
    free(trk->name);
    free(trk);
}

//Returns a string representation of a track.
char * trackToString(void * data) {
    if (data == NULL) {
        return NULL;
    }
    Track * trk = (Track *)(data);
    int mallocSize = 100;
    char * trkstr = malloc(mallocSize);
    trkstr[0] = '\0';
    dynamicAppend(&trkstr, "\n====", &mallocSize);
    dynamicAppend(&trkstr, trk->name, &mallocSize);
    dynamicAppend(&trkstr, "====\n", &mallocSize);

    char * datastr = toString(trk->otherData);
    dynamicAppend(&trkstr, datastr, &mallocSize);
    free(datastr);

    char * segstr = toString(trk->segments);
    dynamicAppend(&trkstr, segstr, &mallocSize);
    free(segstr);


    return trkstr;
}

//Returns 1 if tracks have the same name.
int compareTracks(const void * first, const void * second) {
    if (first == NULL || second == NULL) {
        return -1;
    }
    Track * trk1 = (Track *)(first);
    Track * trk2 = (Track *)(second);
    if (strcmp(trk1->name, trk2->name) == 0) {
        return 1;
    }
    return 0;
}

//Does nothing.
void dummyDelete(void * data) {
    return;
}
/***************************************************************************************/


/********************************* Summary Functions ***********************************/
//Returns the number of waypoints in the GPX file.
int getNumWaypoints(const GPXdoc * doc) {
    if (doc == NULL) {
        return 0;
    }
    return(getLength(doc->waypoints));
}

//Returns the number of routes in the GPX file.
int getNumRoutes(const GPXdoc * doc) {
    if (doc == NULL) {
        return 0;
    }
    return(getLength(doc->routes));
}

//Returns the number of tracks in the GPX file.
int getNumTracks(const GPXdoc * doc) {
    if (doc == NULL) {
        return 0;
    }
    return(getLength(doc->tracks));
}

//Returns the number of segments in all tracks in the document.
int getNumSegments(const GPXdoc * doc) {
    if (doc == NULL) {
        return 0;
    }
    int total = 0;
    void * track;
    ListIterator iter = createIterator(doc->tracks);
    while((track = nextElement(&iter)) != NULL) {
        total += getLength(((Track *)(track))->segments);
    }
    return total;
}

//Returns the number of GPXData elements in the document.
int getNumGPXData(const GPXdoc * doc) {
    if (doc == NULL) {
        return 0;
    }
    int total = 0;
    void * track;
    void * waypoint;
    void * route;
    void * segment;

    //Waypoints
    ListIterator iter = createIterator(doc->waypoints);
    while((waypoint = nextElement(&iter)) != NULL) {
        Waypoint * wpt = (Waypoint *)(waypoint);
        total += getLength(wpt->otherData);
        if (wpt->name[0] != '\0') {
            total++;
        }
    }
    //Routes
    iter = createIterator(doc->routes);
    while((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        total += getLength(rte->otherData);
        if (rte->name[0] != '\0') {
            total++;
        }
        ListIterator iter2 = createIterator(rte->waypoints);
        while((waypoint = nextElement(&iter2)) != NULL) {
            Waypoint * wpt = (Waypoint *)(waypoint);
            total += getLength(wpt->otherData);
            if (wpt->name[0] != '\0') {
                total++;
            }
        }

    }
    //Tracks
    iter = createIterator(doc->tracks);
    while((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        total += getLength(trk->otherData);
        if (trk->name[0] != '\0') {
            total++;
        }
        ListIterator iter2 = createIterator(trk->segments);
        while ((segment = nextElement(&iter2)) != NULL) {
            TrackSegment * seg = (TrackSegment *)(segment);
            ListIterator iter3 = createIterator(seg->waypoints);
            while ((waypoint = nextElement(&iter3)) != NULL) {
                Waypoint * wpt = (Waypoint *)(waypoint);
                total += getLength(wpt->otherData);
                if (wpt->name[0] != '\0') {
                    total++;
                }
            }
        }
    }
    return total;
}
/***************************************************************************************/


/********************************** Getter functions ***********************************/
//Returns a waypoint of a given name.
Waypoint * getWaypoint(const GPXdoc * doc, char * name) {
    if (doc == NULL || name == NULL) {
        return NULL;
    }
    ListIterator iter = createIterator(doc->waypoints);
    void * waypoint;
    while ((waypoint = nextElement(&iter)) != NULL) {
        Waypoint * wpt = (Waypoint *)(waypoint);
        if (strcmp(wpt->name, name) == 0) {
            return wpt;
        }
    }
    return NULL;
}

//Returns a track of a given name.
Track * getTrack(const GPXdoc * doc, char * name) {
    if (doc == NULL || name == NULL) {
        return NULL;
    }
    ListIterator iter = createIterator(doc->tracks);
    void * track;
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        if (strcmp(trk->name, name) == 0) {
            return trk;
        }
    }
    return NULL;
}

//Returns a route of a given name.
Route * getRoute(const GPXdoc * doc, char * name) {
    if (doc == NULL || name == NULL) {
        return NULL;
    }
    ListIterator iter = createIterator(doc->routes);
    void * route;
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        if (strcmp(rte->name, name) == 0) {
            return rte;
        }
    }
    return NULL;
}
/***************************************************************************************/


/****************************** Parser helper functions ********************************/
//Creates a GPXData struct from an xmlNode and appends it to the given list.
void addGpxDataToList(List * dataList, xmlNode * dataNode) {
    xmlChar * value = xmlNodeGetContent(dataNode);
    GPXData * newData = malloc(sizeof(GPXData) + strlen((char *)(value)) + 1);
    strcpy(newData->name, (char *)(dataNode->name));
    strcpy(newData->value, (char *)(value));
    xmlFree(value);
    insertBack(dataList, newData);
}

//Creates a waypoint from an xmlNode and appends it to the given list.
void addWaypointToList(List * wptList, xmlNode * wptNode) {
    Waypoint * newWpt = malloc(sizeof(Waypoint));
    newWpt->otherData = initializeList(*gpxDataToString, *deleteGpxData, *compareGpxData);
    newWpt->name = NULL;
    xmlChar * lat = xmlGetProp(wptNode, (xmlChar *)("lat"));
    xmlChar * lon = xmlGetProp(wptNode, (xmlChar *)("lon"));
    newWpt->latitude = strtod((char *)lat, NULL);
    newWpt->longitude = strtod((char *)lon, NULL);
    xmlFree(lat);
    xmlFree(lon);
    xmlNode * childData = wptNode->children;
    while(childData != NULL) {
        if (strcmp((char *)childData->name, "name") == 0) {
            xmlChar * name = xmlNodeGetContent(childData);
            newWpt->name = malloc(strlen((char *)(name)) + 1);
            newWpt->name[0] = '\0';
            strcpy(newWpt->name, (char *)(name));
            xmlFree(name);
        } else if (strcmp((char *)childData->name, "text") != 0) {
            addGpxDataToList(newWpt->otherData, childData);
        }
        childData = childData->next;
    }
    if (newWpt->name == NULL) {
        newWpt->name = malloc(1);
        strcpy(newWpt->name, "");
    }
    insertBack(wptList, newWpt);
}

//Creates a track segment from an xmlNode and appends it to the given list.
void addSegmentToList(List * segList, xmlNode * segNode) {
    TrackSegment * newSeg = malloc(sizeof(TrackSegment));
    newSeg->waypoints = initializeList(*waypointToString, *deleteWaypoint, *compareWaypoints);
    xmlNode * wptNode = segNode->children;
    while (wptNode != NULL) {
        if (strcmp((char *)wptNode->name, "trkpt") == 0) {
            addWaypointToList(newSeg->waypoints, wptNode);
        }
        wptNode = wptNode->next;
    }
    insertBack(segList, newSeg);
}

//Creates a track from an xmlNode and appends it to the given list.
void addTrackToList(List * trkList, xmlNode * trkNode) {
    Track * newTrack = malloc(sizeof(Track));
    newTrack->segments = initializeList(*trackSegmentToString, *deleteTrackSegment, *compareTrackSegments);
    newTrack->otherData = initializeList(*gpxDataToString, *deleteGpxData, *compareGpxData);
    newTrack->name = NULL;
    xmlNode * childNode = trkNode->children;
    while (childNode != NULL) {
        if (strcmp((char *)childNode->name, "trkseg") == 0) {
            addSegmentToList(newTrack->segments, childNode);
        } else if (strcmp((char *)childNode->name, "name") == 0) {
            xmlChar * name = xmlNodeGetContent(childNode);
            newTrack->name = malloc(strlen((char *)(name)) + 1);
            newTrack->name[0] = '\0';
            strcpy(newTrack->name, (char *)(name));
            xmlFree(name);
        } else if (strcmp((char *)childNode->name, "text") != 0) {
            addGpxDataToList(newTrack->otherData, childNode);
        }
        childNode = childNode->next;
    }
    if (newTrack->name == NULL) {
        newTrack->name = malloc(1);
        strcpy(newTrack->name, "");
    }
    insertBack(trkList, newTrack);
}

//Creates a route from an xmlNode and appends it to the given list.
void addRouteToList(List * rteList, xmlNode * rteNode) {
    Route * newRoute = malloc(sizeof(Route));
    newRoute->waypoints = initializeList(*waypointToString, *deleteWaypoint, *compareWaypoints);
    newRoute->otherData = initializeList(*gpxDataToString, *deleteGpxData, *compareGpxData);
    newRoute->name = NULL;
    xmlNode * wptNode = rteNode->children;
    while (wptNode != NULL) {
        if (strcmp((char *)wptNode->name, "rtept") == 0) {
            addWaypointToList(newRoute->waypoints, wptNode);
        } else if (strcmp((char *)wptNode->name, "name") == 0) {
            xmlChar * name = xmlNodeGetContent(wptNode);
            newRoute->name = malloc(strlen((char *)(name)) + 1);
            newRoute->name[0] = '\0';
            strcpy(newRoute->name, (char *)(name));
            xmlFree(name);
        } else if (strcmp((char *)wptNode->name, "text") != 0) {
            addGpxDataToList(newRoute->otherData, wptNode);
        }
        wptNode = wptNode->next;
    }
    if (newRoute->name == NULL) {
        newRoute->name = malloc(1);
        strcpy(newRoute->name, "");
    }
    insertBack(rteList, newRoute);
}

//Parses the tracks of a GPXdoc.
void setTracks(GPXdoc * doc, xmlNode * root) {
    List * trkList = initializeList(*trackToString, *deleteTrack, *compareTracks);
    xmlNode * current = root->children;
    while (current != NULL) {
        if (strcmp((char *)(current->name), "trk") == 0) {
            addTrackToList(trkList, current);
        }
        current = current->next;
    }
    doc->tracks = trkList;
}

//Parses the routes of a GPXdoc.
void setRoutes(GPXdoc * doc, xmlNode * root) {
    List * rteList = initializeList(*routeToString, *deleteRoute, *compareRoutes);
    xmlNode * current = root->children;
    while (current != NULL) {
        if (strcmp((char *)(current->name), "rte") == 0) {
            addRouteToList(rteList, current);
        }
        current = current->next;
    }
    doc->routes = rteList;
}

//Parses the waypoints of a GPXdoc.
void setWaypoints(GPXdoc * doc, xmlNode * root) {
    List * wptList = initializeList(*waypointToString, *deleteWaypoint, *compareWaypoints);
    xmlNode * current = root->children;
    while (current != NULL) {
        if (strcmp((char *)(current->name), "wpt") == 0) {
            addWaypointToList(wptList, current);
        }
        current = current->next;
    }
    doc->waypoints = wptList;
}

//Parses the namespace, creator, and verion of a GPXdoc.
void setMetadata(GPXdoc * doc, xmlNode * root) {
    xmlAttr * attribute = root->properties;
    strcpy(doc->namespace, (char *)root->ns->href);
    while (attribute != NULL) {
        xmlNode * value = attribute->children;
        char * attributeName = (char *)attribute->name;
        if (strcmp(attributeName, "creator") == 0) {
            doc->creator = malloc(strlen((char *)(value->content)) + 1);
            strcpy(doc->creator, (char *)(value->content));
        } else if (strcmp(attributeName, "version") == 0) {
            doc->version = strtod((char *)(value->content), NULL);
        }
        attribute = attribute->next;
    }
}
/***************************************************************************************/


/********************************** Parser functions ***********************************/
//Frees a GPXdoc structure.
void deleteGPXdoc(GPXdoc * doc) {
    if (doc == NULL) {
        return;
    }
    free(doc->creator);
    freeList(doc->waypoints);
    freeList(doc->routes);
    freeList(doc->tracks);
    free(doc);
}

//Returns a string representation of a GPXdoc.
char * GPXdocToString(GPXdoc* doc) {
    if (doc == NULL) {
        return NULL;
    }
    int mallocSize = 100;
    char * gpxstr = malloc(mallocSize);
    if (gpxstr == NULL) {
        return NULL;
    }
    gpxstr[0] = '\0';

    dynamicAppend(&gpxstr, "Namespace: ", &mallocSize);
    dynamicAppend(&gpxstr, (char *)(doc->namespace), &mallocSize);
    dynamicAppend(&gpxstr, "\n", &mallocSize);

    dynamicAppend(&gpxstr, "Creator: ", &mallocSize);
    dynamicAppend(&gpxstr, (char *)(doc->creator), &mallocSize);
    dynamicAppend(&gpxstr, "\n", &mallocSize);

    dynamicAppend(&gpxstr, "Version: ", &mallocSize);
    dynamicAppendDouble(&gpxstr, doc->version, &mallocSize);
    dynamicAppend(&gpxstr, "\n\n", &mallocSize);

    char * wptstr = toString(doc->waypoints);
    dynamicAppend(&gpxstr, wptstr, &mallocSize);
    free(wptstr);

    char * rtestr = toString(doc->routes);
    dynamicAppend(&gpxstr, rtestr, &mallocSize);
    free(rtestr);

    char * trkstr = toString(doc->tracks);
    dynamicAppend(&gpxstr, trkstr, &mallocSize);
    free(trkstr);
    return gpxstr;
}

//Creates a new GPXdoc from a given GPX filename.
GPXdoc * createGPXdoc(char * fileName) {
    if (fileName == NULL) {
        return NULL;
    }
    GPXdoc * newGPXdoc;
    xmlDoc * newXMLdoc = NULL;
    xmlNode * rootNode = NULL;
    LIBXML_TEST_VERSION
    newXMLdoc = xmlReadFile(fileName, NULL, 0);
    if (newXMLdoc == NULL) {
        xmlFreeDoc(newXMLdoc);
        xmlCleanupParser();
        return NULL;
    }
    rootNode = xmlDocGetRootElement(newXMLdoc);
    newGPXdoc = malloc(sizeof(GPXdoc));
    if (newGPXdoc == NULL) {
        xmlFreeDoc(newXMLdoc);
        xmlCleanupParser();
        return NULL;
    }
    setMetadata(newGPXdoc, rootNode);
    setWaypoints(newGPXdoc, rootNode);
    setRoutes(newGPXdoc, rootNode);
    setTracks(newGPXdoc, rootNode);
    xmlFreeDoc(newXMLdoc);
    xmlCleanupParser();
    return newGPXdoc;
}
/***************************************************************************************/


/**************************** Validation helper functions *******************************/
//Frees pointers associtated with validateXmlDoc.
void cleanupParser(xmlSchema * gpxschema, xmlSchemaParserCtxt * parserctxt, xmlSchemaValidCtxt * validctxt) {
    if (gpxschema != NULL) {
        xmlSchemaFree(gpxschema);
    }
    if (parserctxt != NULL) {
        xmlSchemaFreeParserCtxt(parserctxt);
    }
    if (validctxt != NULL) {
        xmlSchemaFreeValidCtxt(validctxt);
    }
}

//Returns true if the given GPXData list violates the conditions in GPXParser.h
bool GPXDataViolateConstraints(List * gpxList) {
    ListIterator iter;
    void * data;
    iter = createIterator(gpxList);
    while ((data = nextElement(&iter)) != NULL) {
        GPXData * gpx = (GPXData *)(data);
        if (gpx->name[0] == '\0' || gpx->value[0] == '\0') {
            return true;
        }
    }
    return false;
}

//Returns true if the given Waypoint list violates the conditions in GPXParser.h
bool waypointsViolateConstraints(List * wptList) {
    ListIterator iter;
    void * waypoint;
    iter = createIterator(wptList);
    while ((waypoint = nextElement(&iter)) != NULL) {
        Waypoint * wpt = (Waypoint *)(waypoint);
        if (wpt->name == NULL || wpt->otherData == NULL) {
            return true;
        }
        if (GPXDataViolateConstraints(wpt->otherData)) {
            return true;
        }
    }
    return false;
}

//Returns true if the given Route list violates the conditions in GPXParser.h
bool routesViolateConstraints(List * rteList) {
    ListIterator iter;
    void * route;
    iter = createIterator(rteList);
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        if (rte->name == NULL || rte->waypoints == NULL || rte->otherData == NULL) {
            return true;
        }
        if (waypointsViolateConstraints(rte->waypoints)) {
            return true;
        }
        if (GPXDataViolateConstraints(rte->otherData)) {
            return true;
        }
    }
    return false;
}

//Returns true if the given Track list violates the conditions in GPXParser.h
bool tracksViolateConstraints(List * trkList) {
    ListIterator iter;
    void * track;
    void * segment;
    iter = createIterator(trkList);
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        if (trk->name == NULL || trk->segments == NULL || trk->otherData == NULL) {
            return true;
        }
        ListIterator iter2 = createIterator(trk->segments);
        while ((segment = nextElement(&iter2)) != NULL) {
            TrackSegment * seg = (TrackSegment *)(segment);
            if (waypointsViolateConstraints(seg->waypoints)) {
                return true;
            }
        }
    }
    return false;
}

//Returns true if the given GPXdoc violates the conditions in GPXParser.h
bool docViolatesConstraints(GPXdoc * doc) {
    if (doc->namespace == NULL || doc->namespace[0] == '\0')                  return true;
    if (doc->creator   == NULL || doc->creator[0] == '\0')                    return true;
    if (doc->waypoints == NULL || doc->routes == NULL || doc->tracks == NULL) return true;
    if (waypointsViolateConstraints(doc->waypoints)) {
        return true;
    }
    if (routesViolateConstraints(doc->routes)) {
        return true;
    }
    if (tracksViolateConstraints(doc->tracks)) {
        return true;
    }
    return false;
}

//Sets creator, version, and namespace of the given node.
void xmlNodeSetMetadata(GPXdoc * doc, xmlNode * root) {
    char version[50];
    sprintf(version, "%g", doc->version);
    xmlNewProp(root, (const xmlChar *)"creator", (const xmlChar *)doc->creator);
    xmlNewProp(root, (const xmlChar *)"version", (const xmlChar *)version);
    xmlNs * ns = xmlNewNs(root, (const xmlChar *)doc->namespace, NULL);
    xmlSetNs(root, ns);
}

//Adds gpx data in gpxList to the given xmlNode.
void xmlNodeAddGPXData(List * gpxList, xmlNode * root) {
    ListIterator iter;
    void * data;
    iter = createIterator(gpxList);
    while ((data = nextElement(&iter)) != NULL) {
        GPXData * gpx = (GPXData *)(data);
        xmlNewChild(root, NULL, (const xmlChar *)gpx->name, (const xmlChar *)gpx->value);
    }
}

//Adds waypoints in wptList to the given xmlNode.
void xmlNodeAddWaypoints(List * wptList, xmlNode * root, const xmlChar * type) {
    char lat[50];
    char lon[50];
    void * waypoint;
    ListIterator iter = createIterator(wptList);
    while((waypoint = nextElement(&iter)) != NULL) {
        Waypoint * wpt = (Waypoint *)(waypoint);
        sprintf(lat, "%.6lf", wpt->latitude);
        sprintf(lon, "%.6lf", wpt->longitude);
        xmlNode * newWptNode = xmlNewChild(root, NULL, (const xmlChar *)type, NULL);
        xmlNewProp(newWptNode, (const xmlChar *)"lat", (const xmlChar *)lat);
        xmlNewProp(newWptNode, (const xmlChar *)"lon", (const xmlChar *)lon);
        if (wpt->name[0] != '\0') {
            xmlNewChild(newWptNode, NULL, (const xmlChar *)"name", (const xmlChar *)wpt->name);
        }
        xmlNodeAddGPXData(wpt->otherData, newWptNode);
    }
}

//Adds routes in rteList to the given xmlNode.
void xmlNodeAddRoutes(List * rteList, xmlNode * root) {
    ListIterator iter;
    void * route;
    iter = createIterator(rteList);
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        xmlNode * newRteNode = xmlNewChild(root, NULL, (const xmlChar *)"rte", NULL);
        if (rte->name[0] != '\0') {
            xmlNewChild(newRteNode, NULL, (const xmlChar *)"name", (const xmlChar *)rte->name);
        }
        xmlNodeAddGPXData(rte->otherData, newRteNode);
        xmlNodeAddWaypoints(rte->waypoints, newRteNode, (const xmlChar *)"rtept");
    }
}

//Adds tracks in trkList to the given xmlNode.
void xmlNodeAddTracks(List * trkList, xmlNode * root) {
    ListIterator iter;
    void * track;
    iter = createIterator(trkList);
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        xmlNode * newTrkNode = xmlNewChild(root, NULL, (const xmlChar *)"trk", NULL);
        if (trk->name[0] != '\0') {
            xmlNewChild(newTrkNode, NULL, (const xmlChar *)"name", (const xmlChar *)trk->name);
        }
        xmlNodeAddGPXData(trk->otherData, newTrkNode);
        ListIterator iter2;
        void * segment;
        iter2 = createIterator(trk->segments);
        while ((segment = nextElement(&iter2)) != NULL) {
            TrackSegment * seg = (TrackSegment *)(segment);
            xmlNode * newSegNode = xmlNewChild(newTrkNode, NULL, (const xmlChar *)"trkseg", NULL);
            xmlNodeAddWaypoints(seg->waypoints, newSegNode, (const xmlChar *)"trkpt");
        }
    }
}

//Creates an xmlDoc from a GPXdoc.
xmlDoc * createXMLdoc(GPXdoc * doc) {
    xmlDoc * newDoc = xmlNewDoc((const xmlChar *)XML_DEFAULT_VERSION);
    xmlNode * rootNode = xmlNewNode(NULL, (const xmlChar *)"gpx");
    xmlNodeSetMetadata(doc, rootNode);
    xmlNodeAddWaypoints(doc->waypoints, rootNode, (const xmlChar *)"wpt");
    xmlNodeAddRoutes(doc->routes, rootNode);
    xmlNodeAddTracks(doc->tracks, rootNode);
    xmlDocSetRootElement(newDoc, rootNode);
    return newDoc;
}

//Validates an xmlDoc against a schema file.
int validateXmlDoc(xmlDoc * doc, char * gpxSchemaFile) {
    xmlSchema * gpxschema = NULL;
    xmlSchemaParserCtxt * parserctxt = NULL;
    xmlSchemaValidCtxt * validctxt = NULL;

    //xmlSchemaNewParserCtxt does not return NULL if the file does not exist so check explicitly here
    if (access(gpxSchemaFile, F_OK) != 0) {
        return -1;
    }
    parserctxt = xmlSchemaNewParserCtxt(gpxSchemaFile);
    if (parserctxt == NULL) {
        cleanupParser(gpxschema, parserctxt, validctxt);
        return -1;
    }
    xmlSchemaSetParserErrors(parserctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
    gpxschema = xmlSchemaParse(parserctxt);
    if (gpxschema == NULL) {
        cleanupParser(gpxschema, parserctxt, validctxt);
        return -1;
    }
    validctxt = xmlSchemaNewValidCtxt(gpxschema);
        if (validctxt == NULL) {
        cleanupParser(gpxschema, parserctxt, validctxt);
        return -1;
    }
    xmlSchemaSetValidErrors(validctxt, (xmlSchemaValidityErrorFunc) fprintf, (xmlSchemaValidityWarningFunc) fprintf, stderr);
    int valid = xmlSchemaValidateDoc(validctxt, doc);
    cleanupParser(gpxschema, parserctxt, validctxt);
    return valid;
}
/***************************************************************************************/


/********************************* Valiation functions **********************************/
//Validates a GPX file then creates a GPXdoc.
GPXdoc * createValidGPXdoc(char * fileName, char * gpxSchemaFile) {
    if (fileName == NULL || gpxSchemaFile == NULL) {
        return NULL;
    }
    LIBXML_TEST_VERSION
    xmlDoc * newXMLdoc = NULL;
    newXMLdoc = xmlReadFile(fileName, NULL, 0);
    if (newXMLdoc == NULL) {
        xmlFreeDoc(newXMLdoc);
        xmlCleanupParser();
        return NULL;
    }
    int valid = validateXmlDoc(newXMLdoc, gpxSchemaFile);
    xmlFreeDoc(newXMLdoc);
    xmlCleanupParser();
    if (valid == 0) {
        return createGPXdoc(fileName);
    }
    return NULL;
}

//Validates a GPXdox against the constraints in GPXParser.h and the given schema file
bool validateGPXDoc(GPXdoc * doc, char * gpxSchemaFile) {
    if (doc == NULL || gpxSchemaFile == NULL) {
        return false;
    }
    LIBXML_TEST_VERSION
    if (docViolatesConstraints(doc)) return false;
    xmlDoc * newDoc = NULL;
    newDoc = createXMLdoc(doc);
    int valid = validateXmlDoc(newDoc, gpxSchemaFile);
    xmlFreeDoc(newDoc);
    xmlCleanupParser();
    if (valid == 0) {
        return true;
    }
    return false;
}

//Wites a GPXdoc to a gpx file
bool writeGPXdoc(GPXdoc * doc, char * fileName) {
    if (doc == NULL || fileName == NULL) {
        return false;
    }
    LIBXML_TEST_VERSION
    xmlDoc * newDoc = NULL;
    newDoc = createXMLdoc(doc);
    if (doc == NULL) {
        xmlFreeDoc(newDoc);
        xmlCleanupParser();
        return false;
    }
    xmlSaveFormatFileEnc(fileName, newDoc, "UTF-8", 1);
    xmlFreeDoc(newDoc);
    xmlCleanupParser();
    return true;
}
/***************************************************************************************/


/********************************* Distance functions **********************************/
//Computes the distatnce between two waypoints using the haversine formula.
float haversine(Waypoint * start, Waypoint * end) {
    float delta_lat = (end->latitude - start->latitude) * M_PI / 180;
    float delta_lon = (end->longitude - start->longitude) * M_PI / 180;
    float a = sin(delta_lat / 2) * sin(delta_lat / 2) + cos(start->latitude * M_PI / 180) * cos(end->latitude * M_PI / 180) * sin(delta_lon / 2) * sin(delta_lon / 2);
    float c = 2 * atan2(sqrt(a), sqrt(1 - a));
    return EARTH_RADIUS * c;
}

//Rounds a float to the nearest multiple of 10.
float round10(float len) {
    return round(len / 10.0) * 10.0;
}

//Returns the length of a route.
float getRouteLen(const Route * rt) {
    if (rt == NULL) {
        return 0;
    }
    float total_len = 0;
    void * prev_waypoint = NULL;
    void * curr_waypoint = NULL;
    ListIterator iter;
    iter = createIterator(rt->waypoints);
    while ((curr_waypoint = nextElement(&iter)) != NULL) {
        if (prev_waypoint != NULL) {
            Waypoint * prev_wpt = (Waypoint *)(prev_waypoint);
            Waypoint * curr_wpt = (Waypoint *)(curr_waypoint);
            total_len += haversine(prev_wpt, curr_wpt);
        }
        prev_waypoint = curr_waypoint;
    }
    return total_len;
}

//Returns the length of a track.
float getTrackLen(const Track * tr) {
    if (tr == 0) {
        return 0;
    }
    float total_len = 0;
    void * prev_waypoint = NULL;
    void * curr_waypoint = NULL;
    void * curr_segment = NULL;
    ListIterator iter;
    iter = createIterator(tr->segments);
    while ((curr_segment = nextElement(&iter)) != NULL) {
        TrackSegment * seg = (TrackSegment *)(curr_segment);
        ListIterator iter2 = createIterator(seg->waypoints);
        while ((curr_waypoint = nextElement(&iter2)) != NULL) {
            if (prev_waypoint != NULL) {
                Waypoint * prev_wpt = (Waypoint *)(prev_waypoint);
                Waypoint * curr_wpt = (Waypoint *)(curr_waypoint);
                total_len += haversine(prev_wpt, curr_wpt);
            }
            prev_waypoint = curr_waypoint;
        }
    }
    return total_len;
}

//Returns the number of routes with a certain length in a GPXdoc.
int numRoutesWithLength(const GPXdoc * doc, float len, float delta) {
    if (doc == NULL || len < 0 || delta < 0) {
        return 0;
    }
    int num_routes = 0;
    void * route;
    ListIterator iter;
    iter = createIterator(doc->routes);
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        if (fabs(len - getRouteLen(rte)) <= delta) {
            num_routes++;
        }
    }
    return num_routes;
}

//Returns the number of tracks with a certain lenght in a GPXdoc.
int numTracksWithLength(const GPXdoc * doc, float len, float delta) {
    if (doc == NULL || len < 0 || delta < 0) {
        return 0;
    }
    int num_tracks = 0;
    void * track;
    ListIterator iter;
    iter = createIterator(doc->tracks);
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        if (fabs(len - getTrackLen(trk)) <= delta) {
            num_tracks++;
        }
    }
    return num_tracks;
}

//Returns true if a route is a loop.
bool isLoopRoute(const Route * rt, float delta) {
    if (rt == NULL || delta < 0) {
        return false;
    }
    Waypoint * start = getFromBack(rt->waypoints);
    Waypoint * end = getFromFront(rt->waypoints);
    if (haversine(start, end) <= delta) {
        return true;
    }
    return false;
}

//Retruns true if a track is a loop.
bool isLoopTrack(const Track * tr, float delta) {
    if (tr == NULL || delta < 0) {
        return false;
    }
    TrackSegment * startSegment = getFromFront(tr->segments);
    TrackSegment * endSegment = getFromBack(tr->segments);
    Waypoint * start = getFromFront(startSegment->waypoints);
    Waypoint * end = getFromBack(endSegment->waypoints);
    if (haversine(start, end) <= delta) {
        return true;
    }
    return false;
}

//Returns all the routes that connect the given points.
List * getRoutesBetween(const GPXdoc * doc, float sourceLat, float sourceLong, float destLat, float destLong, float delta) {
    if (doc == NULL || delta < 0) {
        return NULL;
    }
    Waypoint * sourceStart = malloc(sizeof(Waypoint));
    Waypoint * sourceEnd = malloc(sizeof(Waypoint));
    List * rteList = initializeList(*routeToString, *dummyDelete, *compareRoutes);
    bool hasElement = false;
    void * route;
    sourceStart->latitude = sourceLat;
    sourceStart->longitude = sourceLong;
    sourceStart->name = NULL;
    sourceStart->otherData = NULL;
    sourceEnd->latitude = destLat;
    sourceEnd->longitude = destLong;
    sourceEnd->name = NULL;
    sourceEnd->otherData = NULL;
    ListIterator iter;
    iter = createIterator(doc->routes);
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        Waypoint * start = getFromFront(rte->waypoints);
        Waypoint * end = getFromBack(rte->waypoints);
        if ((haversine(sourceStart, start)) <= delta && (haversine(sourceEnd, end) <= delta)) {
            insertBack(rteList, rte);
            hasElement = true;
        }
    }
    deleteWaypoint(sourceStart);
    deleteWaypoint(sourceEnd);
    if (hasElement) {
        return rteList;
    }
    freeList(rteList);
    return NULL;
}

//Returns all the tracks that connect the given points.
List * getTracksBetween(const GPXdoc * doc, float sourceLat, float sourceLong, float destLat, float destLong, float delta) {
    if (doc == NULL || delta < 0) {
        return NULL;
    }
    Waypoint * sourceStart = malloc(sizeof(Waypoint));
    Waypoint * sourceEnd = malloc(sizeof(Waypoint));
    List * trkList = initializeList(*trackToString, *dummyDelete, *compareTracks);
    bool hasElement = false;
    void * track;
    sourceStart->latitude = sourceLat;
    sourceStart->longitude = sourceLong;
    sourceStart->name = NULL;
    sourceStart->otherData = NULL;
    sourceEnd->latitude = destLat;
    sourceEnd->longitude = destLong;
    sourceEnd->name = NULL;
    sourceEnd->otherData = NULL;
    ListIterator iter;
    iter = createIterator(doc->tracks);
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        TrackSegment * startSegment = getFromFront(trk->segments);
        TrackSegment * endSegment = getFromBack(trk->segments);
        Waypoint * start = getFromFront(startSegment->waypoints);
        Waypoint * end = getFromBack(endSegment->waypoints);
        if ((haversine(sourceStart, start)) <= delta && (haversine(sourceEnd, end) <= delta)) {
            insertBack(trkList, trk);
            hasElement = true;
        }
    }
    deleteWaypoint(sourceStart);
    deleteWaypoint(sourceEnd);
    if (hasElement) {
        return trkList;
    }
    freeList(trkList);
    return NULL;
}
/***************************************************************************************/


/*********************************** JSON functions ************************************/
//Converts a route to a simplified JSON format.
char * routeToJSON(const Route * rt) {
    char * jsonStr;
    if (rt == NULL) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "{}");
        return jsonStr;
    }
    int numPoints = getLength(rt->waypoints);
    float len = round10(getRouteLen(rt));
    bool isLoop = isLoopRoute(rt, 10.0);
    jsonStr = malloc(100 + strlen(rt->name));
    sprintf(jsonStr, "{\"name\":\"%s\",\"numPoints\":%d,\"len\":%.1f,\"loop\":%s}", rt->name[0] == '\0' ? "None":rt->name, numPoints, len, isLoop ? "true":"false");
    return jsonStr;
}

//Converts a track to a simplified JSON format.
char * trackToJSON(const Track * tr) {
    char * jsonStr;
    if (tr == NULL) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "{}");
        return jsonStr;
    }
    float len = round10(getTrackLen(tr));
    bool isLoop = isLoopTrack(tr, 10.0);
    int numPoints = 0;
    void * segment;
    ListIterator iter = createIterator(tr->segments);
    while ((segment = nextElement(&iter)) != NULL) {
        TrackSegment * seg = (TrackSegment *)(segment);
        numPoints += getLength(seg->waypoints);
    }
    jsonStr = malloc(100 + strlen(tr->name));
    sprintf(jsonStr, "{\"name\":\"%s\",\"numPoints\":%d,\"len\":%.1f,\"loop\":%s}", tr->name[0] == '\0' ? "None":tr->name, numPoints, len, isLoop ? "true":"false");
    return jsonStr;
}

//Converts GPXData to a simplified JSON format.
char * gpxDataToJSON(const GPXData * data) {
    char * jsonStr;
    if (data == NULL) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "{}");
        return jsonStr;
    }
    jsonStr = malloc(25 + strlen(data->name) + strlen(data->value));
    sprintf(jsonStr, "{\"name\":\"%s\",\"value\":\"%s\"}", data->name, data->value);
    return jsonStr;
}

//Converts a route list to a simplified JSON format.
char * routeListToJSON(const List * list) {
    int mallocSize = 100;
    char * jsonStr;
    if (list == NULL || getLength((List* )(list)) == 0) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "[]");
        return jsonStr;
    }
    void * route;
    jsonStr = malloc(mallocSize);
    jsonStr[0] = '\0';
    dynamicAppend(&jsonStr, "[", &mallocSize);
    ListIterator iter = createIterator((List* )(list));
    while ((route = nextElement(&iter)) != NULL) {
        Route * rte = (Route *)(route);
        char * rteJSON = routeToJSON(rte);
        dynamicAppend(&jsonStr, rteJSON, &mallocSize);
        dynamicAppend(&jsonStr, ",", &mallocSize);
        free(rteJSON);
    }
    jsonStr[strlen(jsonStr) - 1] = ']';
    return jsonStr;
}

//Converts a track list to a simplified JSON format.
char * trackListToJSON(const List * list) {
    int mallocSize = 100;
    char * jsonStr;
    if (list == NULL || getLength((List* )(list)) == 0) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "[]");
        return jsonStr;
    }
    void * track;
    jsonStr = malloc(mallocSize);
    jsonStr[0] = '\0';
    dynamicAppend(&jsonStr, "[", &mallocSize);
    ListIterator iter = createIterator((List* )(list));
    while ((track = nextElement(&iter)) != NULL) {
        Track * trk = (Track *)(track);
        char * trkJSON = trackToJSON(trk);
        dynamicAppend(&jsonStr, trkJSON, &mallocSize);
        dynamicAppend(&jsonStr, ",", &mallocSize);
        free(trkJSON);
    }
    jsonStr[strlen(jsonStr) - 1] = ']';
    return jsonStr;
}

char * gpxListToJSON(const List * list) {
    int mallocSize = 100;
    char * jsonStr;
    if (list == NULL || getLength((List* )(list)) == 0) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "[]");
        return jsonStr;
    }
    void * gpx;
    jsonStr = malloc(mallocSize);
    jsonStr[0] = '\0';
    dynamicAppend(&jsonStr, "[", &mallocSize);
     ListIterator iter = createIterator((List* )(list));
    while ((gpx = nextElement(&iter)) != NULL) {
        GPXData * data = (GPXData *)(gpx);
        char * gpxJSON = gpxDataToJSON(data);
        dynamicAppend(&jsonStr, gpxJSON, &mallocSize);
        dynamicAppend(&jsonStr, ",", &mallocSize);
        free(gpxJSON);
    }
    jsonStr[strlen(jsonStr) - 1] = ']';
    return jsonStr;
}

//Converts a GPXdoc to a simplified JSON format.
char * GPXtoJSON(const GPXdoc * gpx) {
    char * jsonStr;
    if (gpx == NULL) {
        jsonStr = malloc(3);
        strcpy(jsonStr, "{}");
        return jsonStr;
    }
    int numWpt = getLength(gpx->waypoints);
    int numRoutes = getLength(gpx->routes);
    int numTracks = getLength(gpx->tracks);
    jsonStr = malloc(100 + strlen(gpx->creator));
    sprintf(jsonStr, "{\"version\":%g,\"creator\":\"%s\",\"numWaypoints\":%d,\"numRoutes\":%d,\"numTracks\":%d}", gpx->version, gpx->creator, numWpt, numRoutes, numTracks);
    return jsonStr;
}
/***************************************************************************************/


/******************************** Wrapper functions *************************************/
//Given a file returns the JSON form of a GPXdoc.
char * fileToJSON(char * fileName) {
    GPXdoc * doc = createGPXdoc(fileName);
    char * jsonStr = GPXtoJSON(doc);
    deleteGPXdoc(doc);
    return jsonStr;
}

//Returns a list of all routes/tracks from a GPX file.
char * fileToPathList(char * fileName) {
    GPXdoc * doc = createGPXdoc(fileName);
    char * routeJson = routeListToJSON(doc->routes);
    char * trackJson = trackListToJSON(doc->tracks);
    int mallocSize = 100;
    char * jsonStr = malloc(mallocSize);
    jsonStr[0] = '\0';
    dynamicAppend(&jsonStr, "[", &mallocSize);
    dynamicAppend(&jsonStr, routeJson, &mallocSize);
    dynamicAppend(&jsonStr, ",", &mallocSize);
    dynamicAppend(&jsonStr, trackJson, &mallocSize);
    dynamicAppend(&jsonStr, "]", &mallocSize);
    free(routeJson);
    free(trackJson);
    deleteGPXdoc(doc);
    return jsonStr;
}

//Validates a GPX file.
bool validateGPXfile(char * fileName, char * schemaFile) {
    LIBXML_TEST_VERSION
    xmlDoc * newXMLdoc = xmlReadFile(fileName, NULL, 0);
    int valid = validateXmlDoc(newXMLdoc, schemaFile);
    if (valid == 0) {
        xmlFreeDoc(newXMLdoc);
        xmlCleanupParser();
        return true;
    }
    xmlFreeDoc(newXMLdoc);
    xmlCleanupParser();
    return false;
}

//Returns a list of GPX data associated with a path.
char * getGPXdataList(char * fileName, int index, char * mode) {
    GPXdoc * doc = createGPXdoc(fileName);
    void * path;
    char * gpxJson;
    ListIterator iter;
    if (strcmp(mode, "route") == 0) {
        iter = createIterator(doc->routes);
        int i = 0;
        while ((path = nextElement(&iter)) != NULL) {
            if (i == index - 1) {
                Route * rte = (Route *)(path);
                gpxJson = gpxListToJSON(rte->otherData);
                deleteGPXdoc(doc);
                return gpxJson;
            }
            i++;
        }
    } else {
        iter = createIterator(doc->tracks);
        int i = 0;
        while ((path = nextElement(&iter)) != NULL) {
            if (i == index - 1) {
                Track * trk = (Track *)(path);
                gpxJson = gpxListToJSON(trk->otherData);
                deleteGPXdoc(doc);
                return gpxJson;
            }
            i++;
        }
    }
    deleteGPXdoc(doc);
    gpxJson = malloc(3);
    strcpy(gpxJson, "[]");
    return gpxJson;
}

//Renames a specific track/route in a GPX file.
bool renamePath(char * fileName, char * newName, char * schemaFile, int index, char * type) {
    GPXdoc * doc = createValidGPXdoc(fileName, schemaFile);
    if (doc == NULL) {
        return false;
    }
    void * path;
    ListIterator iter;
    if (strcmp(type, "route") == 0) {
        iter = createIterator(doc->routes);
        int i = 0;
        while ((path = nextElement(&iter)) != NULL) {
            if (i == index - 1) {
                Route * rte = (Route *)(path);
                rte->name = realloc(rte->name, strlen(newName) + 1);
                strcpy(rte->name, newName);
                if (validateGPXDoc(doc, schemaFile)) {
                    writeGPXdoc(doc, fileName);
                    deleteGPXdoc(doc);
                    return true;
                } else {
                    deleteGPXdoc(doc);
                    return false;
                }
            }
            i++;
        }
    } else {
        iter = createIterator(doc->tracks);
        int i = 0;
        while ((path = nextElement(&iter)) != NULL) {
            if (i == index - 1) {
                Track * trk = (Track *)(path);
                trk->name = realloc(trk->name, strlen(newName) + 1);
                strcpy(trk->name, newName);
                if (validateGPXDoc(doc, schemaFile)) {
                    writeGPXdoc(doc, fileName);
                    deleteGPXdoc(doc);
                    return true;
                } else {
                    deleteGPXdoc(doc);
                    return false;
                }
            }
            i++;
        }
    }
    deleteGPXdoc(doc);
    return false;
}

//Creates a GPX file with no data.
bool createEmptyGPX(char * schemaFile, char * fileName, double version, char * creator) {
    GPXdoc * doc = malloc(sizeof(GPXdoc));
    doc->routes = initializeList(*routeToString, *deleteRoute, *compareRoutes);
    doc->tracks = initializeList(*trackToString, *deleteTrack, *compareTracks);
    doc->waypoints = initializeList(*waypointToString, *deleteWaypoint, *compareWaypoints);
    strcpy(doc->namespace, "http://www.topografix.com/GPX/1/1");
    doc->creator = malloc(strlen(creator) + 1);
    strcpy(doc->creator, creator);
    doc->version = version;
    bool valid = validateGPXDoc(doc, schemaFile);
    if (!valid) {
        deleteGPXdoc(doc);
        return false;
    }
    valid = writeGPXdoc(doc, fileName);
    deleteGPXdoc(doc);
    return valid;
}

bool addRouteToFile(char * schemaFile, char * fileName, double * waypoints, int num_waypoints) {
    GPXdoc * doc = createValidGPXdoc(fileName, schemaFile);
    if (doc == NULL) {
        return false;
    }
    Route * newRoute = malloc(sizeof(Route));
    newRoute->waypoints = initializeList(*waypointToString, *deleteWaypoint, *compareWaypoints);
    newRoute->otherData = initializeList(*gpxDataToString, *deleteGpxData, *compareGpxData);
    newRoute->name = malloc(1);
    strcpy(newRoute->name, "");
    for (int i = 0; i < num_waypoints * 2; i+= 2) {
        Waypoint * newWpt = malloc(sizeof(Waypoint));
        newWpt->otherData = initializeList(*gpxDataToString, *deleteGpxData, *compareGpxData);
        newWpt->name = malloc(1);
        strcpy(newWpt->name, "");
        newWpt->latitude = waypoints[i];
        newWpt->longitude = waypoints[i + 1];
        insertBack(newRoute->waypoints, newWpt);
    }
    insertBack(doc->routes, newRoute);
    bool success = validateGPXDoc(doc, schemaFile);
    if (success) {
        success = writeGPXdoc(doc, fileName);
        deleteGPXdoc(doc);
        return success;
    }
    deleteGPXdoc(doc);
    return false;
}

char * getAllPathsBetween(char * fileName, float sourceLat, float sourceLon, float destLat, float destLon, float delta) {
    GPXdoc * doc = createGPXdoc(fileName);
    List * routes = getRoutesBetween(doc, sourceLat, sourceLon, destLat, destLon, delta);
    List * tracks = getTracksBetween(doc, sourceLat, sourceLon, destLat, destLon, delta);
    char * routeJson = routeListToJSON(routes);
    char * trackJson = trackListToJSON(tracks);
    int mallocSize = 100;
    char * jsonStr = malloc(mallocSize);
    jsonStr[0] = '\0';
    dynamicAppend(&jsonStr, "[", &mallocSize);
    dynamicAppend(&jsonStr, routeJson, &mallocSize);
    dynamicAppend(&jsonStr, ",", &mallocSize);
    dynamicAppend(&jsonStr, trackJson, &mallocSize);
    dynamicAppend(&jsonStr, "]", &mallocSize);
    freeList(routes);
    freeList(tracks);
    free(routeJson);
    free(trackJson);
    deleteGPXdoc(doc);
    return jsonStr;
}
/***************************************************************************************/
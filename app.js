'use strict'

// C library API
const ffi = require('ffi-napi');

// Express App (Routes)
const express = require("express");
const app = express();
const path = require("path");
const fileUpload = require('express-fileupload');

app.use(fileUpload());
app.use(express.static(path.join(__dirname + '/uploads')));

// Minimization
const fs = require('fs');
const JavaScriptObfuscator = require('javascript-obfuscator');

// Important, pass in port as in `npm run dev 1234`, do not change
const portNum = process.argv[2];

// Send HTML at root, do not change
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

// Send Style, do not change
app.get('/style.css', function (req, res) {
    //Feel free to change the contents of style.css to prettify your Web app
    res.sendFile(path.join(__dirname + '/public/style.css'));
});

// Send obfuscated JS, do not change
app.get('/index.js', function (req, res) {
    fs.readFile(path.join(__dirname + '/public/index.js'), 'utf8', function (err, contents) {
        const minimizedContents = JavaScriptObfuscator.obfuscate(contents, { compact: true, controlFlowFlattening: true });
        res.contentType('application/javascript');
        res.send(minimizedContents._obfuscatedCode);
    });
});

//Respond to POST requests that upload files to uploads/ directory
app.post('/upload', function (req, res) {
    if (!req.files) {
        return res.status(400).send('No files were uploaded.');
    }

    let uploadFile = req.files.uploadFile;

    // Use the mv() method to place the file somewhere on your server
    uploadFile.mv('uploads/' + uploadFile.name, function (err) {
        if (err) {
            return res.status(500).send(err);
        }

        res.redirect('/');
    });
});

//Respond to GET requests for files in the uploads/ directory
app.get('/uploads/:name', function (req, res) {
    fs.stat('uploads/' + req.params.name, function (err, stat) {
        if (err == null) {
            res.sendFile(path.join(__dirname + '/uploads/' + req.params.name));
        } else {
            console.log('Error in file downloading route: ' + err);
            res.send('');
        }
    });
});

let libgpxparser = ffi.Library('./parser/bin/libgpxparser', {
    'fileToJSON': ['string', ['string']],
    'fileToPathList': ['string', ['string']],
    'validateGPXfile': ['bool', ['string', 'string']],
    'getGPXdataList': ['string', ['string', 'int', 'string']],
    'renamePath': ['bool', ['string', 'string', 'string', 'int', 'string']],
    'createEmptyGPX': ['bool', ['string', 'string', 'double', 'string']],
    'addRouteToFile': ['bool', ['string', 'string', 'int *', 'int']],
    'getAllPathsBetween': ['string', ['string', 'float', 'float', 'float', 'float', 'float']]
});

//Returns a JSON array of gpx JSON objects
app.get('/getFiles', function (req, res) {
    let gpxArray = new Array();
    let files = fs.readdirSync('./uploads');
    files.forEach(file => {
        if (file.split('.').pop() == 'gpx' && libgpxparser.validateGPXfile('./uploads/' + file, 'gpx.xsd')) {
            let gpxJSON = JSON.parse(libgpxparser.fileToJSON('./uploads/' + file));
            gpxJSON['file'] = file;
            gpxArray.push(gpxJSON);
        }
    });
    res.send(gpxArray);
});

//Returns a list of all routes/tracks
app.get('/getPathList', function(req, res) {
    let pathListJSON = JSON.parse(libgpxparser.fileToPathList('./uploads/' + req.query.fileName));
    res.send(pathListJSON);
});

//Returns a list of GPX data from a given path
app.get('/getGPXdataList', function(req, res) {
    let fileName = req.query.file;
    let index = req.query.index;
    let type = req.query.type;
    let jsonStr = libgpxparser.getGPXdataList('./uploads/' + fileName, index, type);
    jsonStr = jsonStr.replace(/[\n\r]+/g, '').replace(/\t/g, '').trim();
    res.send(JSON.parse(jsonStr));
});

//Renames a path in a GPX file
app.get('/renamePath', function(req, res) {
    let fileName = req.query.file;
    let newName = req.query.name;
    let index = req.query.index;
    let type = req.query.type;
    let renamed = libgpxparser.renamePath('./uploads/' + fileName, newName, 'gpx.xsd', index, type);
    if (renamed) {
        res.send("true");
    } else {
        res.send("false");
    }
});

app.use(express.urlencoded({ extended: true }))
app.use(express.json());

//Creates an empty GPX file
app.post('/createGPX', function(req, res) {
    let fileName = req.body.fileName;
    let creator = req.body.creator;
    let files = fs.readdirSync('./uploads');
    let valid = true;
    files.forEach(file => {
        if (file == fileName) {
            valid = false;
            res.send(false);
        }
    });
    if (valid) {
        valid = libgpxparser.createEmptyGPX('gpx.xsd', './uploads/' + fileName, 1.1, creator);
        res.send(valid);
    }
});


//Adds a route to a GPX file
app.post('/addRoute', function(req, res) {
    let waypoints = req.body.waypoints;
    let fileName = req.body.fileName;
    let buf = new Float64Array(waypoints);
    let valid = libgpxparser.addRouteToFile('gpx.xsd', './uploads/' + fileName, buf, waypoints.length / 2);
    res.send(valid);
});

//Returns paths between 2 points
app.get('/findPaths', function(req, res) {
    let pathList = new Array();
    pathList[0] = new Array();
    pathList[1] = new Array();
    let startLat = req.query.startLat;
    let startLon = req.query.startLon;
    let endLat = req.query.endLat;
    let endLon = req.query.endLon;
    let delta = req.query.delta;
    let files = fs.readdirSync('./uploads');
    files.forEach(file => {
        if (file.split('.').pop() == 'gpx' && libgpxparser.validateGPXfile('./uploads/' + file, 'gpx.xsd')) {
            let jsonStr = libgpxparser.getAllPathsBetween('./uploads/' + file, startLat, startLon, endLat, endLon, delta);
            jsonStr = JSON.parse(jsonStr);
            jsonStr[0].forEach(route => {
                pathList[0].push(route);
            });
            jsonStr[1].forEach(track => {
                pathList[1].push(track);
            });
        }
    });
    res.send(pathList);
});

app.listen(portNum);
console.log('Running app at CIS2750.socs.uoguelph.ca:' + portNum);
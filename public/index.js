// Put all onload AJAX calls here, and event listeners
$(document).ready(function() {
    //Populate file table
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/getFiles',
        success: function(data) {
            if (data.length == 0) {
                console.log("No valid files on server");
                $('#GPXtable').append('<tr><td colspan="6">No files found</td></tr>');
            } else {
                data.forEach(gpx => {
                    $('#GPXtable').append(
                        `
                        <tr>
                            <td><a href=./${gpx.file}>${gpx.file}</a></td>
                            <td>${gpx.version}</td>
                            <td>${gpx.creator}</td>
                            <td>${gpx.numWaypoints}</td>
                            <td>${gpx.numRoutes}</td>
                            <td>${gpx.numTracks}</td>
                        </tr>
                        `
                    );
                    $('#gpxSelect').append(`<option>${gpx.file}</option>`);
                    $('#gpxSelect2').append(`<option>${gpx.file}</option>`);
                });
            }
        },
        fail: function(error) {
            console.log(error)
        }
    });
});

//Updates the GPX view panel
function updateGPXview(file) {
    $('#selectedGPXtable').show();
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/getPathList',
        data: {
            fileName: file
        },
        success: function(data) {
            var c = 1;
            $('#selectedGPXtable').html(
                `
                <tr>
                    <th>Component</th>
                    <th>Name</th>
                    <th>Number of points</th>
                    <th>Length</th>
                    <th>Loop</th>
                </tr>
                `
            );
            data[0].forEach(route => {
                $('#selectedGPXtable').append(
                    `
                    <tr id="route${c}">
                        <td>Route ${c}<button onclick="showDetails(${c}, 'route')" class="smallBtn"><i class="fa fa-chevron-down" id="route${c}img"></i></button></td>
                        <td contenteditable='true' id="route${c}name">${route.name}</td>
                        <td>${route.numPoints}</td>
                        <td>${route.len}m</td>
                        <td>${route.loop}</td>
                    </tr>
                    `
                );
                let routeName = document.getElementById(`route${c}name`);
                routeName.addEventListener('keypress', e => editName(e, routeName.textContent, routeName.id.replace(/\D/g,''), "route"));
                c++;
            });
            c = 1;
            data[1].forEach(track => {
                $('#selectedGPXtable').append(
                    `
                    <tr id="track${c}">
                        <td>Track ${c}<button onclick="showDetails(${c}, 'track')" class="smallBtn"><i class="fa fa-chevron-down" id="track${c}img"></i></button></td>
                        <td contenteditable='true' id="track${c}name">${track.name}</td>
                        <td>${track.numPoints}</td>
                        <td>${track.len}m</td>
                        <td>${track.loop}</td>
                    </tr>
                    `
                );
                let trackName = document.getElementById(`track${c}name`);
                trackName.addEventListener('keypress', e => editName(e, trackName.textContent, trackName.id.replace(/\D/g,''), "track"));
                c++;
            });
        },
        fail: function(error) {
            console.log(error)
        }
    });
}

//Shows extra gpxData of a path
async function showDetails(index, type) {
    if ($(`#${type}${index}details`).length == 0) {
        let gpxData = await getGPXdataJSON(index, type);
        let newRow = `<tr id="${type}${index}details"><td colspan="5" style="text-align:left">`;
        gpxData.forEach(gpx => {
            newRow += `${gpx.name}: ${gpx.value}<br>`;
        });
        if (gpxData.length == 0) {
            newRow += 'No other data';
        }
        newRow += `</td></tr>`;
        $(newRow).insertAfter($(`#${type}${index}`));
        $(`#${type}${index}img`).addClass('fa-chevron-up').removeClass('fa-chevron-down');
    } else {
        ($(`#${type}${index}details`)).remove();
        $(`#${type}${index}img`).addClass('fa-chevron-down').removeClass('fa-chevron-up');
    }
}

//Gets extra GPX data of a path
async function getGPXdataJSON(index, type) {
    let gpx = $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/getGPXdataList',
        data: {
            file: $('#gpxSelect :selected').text(),
            index: index,
            type: type
        },
        success: function() {
            console.log("Returned GPX data list succesfully");
        },
        fail: function(error) {
            console.log(error);
        }
    });
    return await gpx;
}

//Edits the name of a path
async function editName(event, newName, index, type) {
    if (event.key == 'Enter') {
        event.preventDefault();
        if (newName == '') {
            alert("Name cannot be empty");
            return;
        } else if ((/\\|&/g).test(newName)) {
            alert("Name cannot contain '\\' or '&'");
            return;
        }
        var renamed = false;
        await $.ajax({
            type: 'get',
            dataType: 'text',
            url: '/renamePath',
            data: {
                file: $('#gpxSelect :selected').text(),
                name: newName,
                index: index,
                type: type
            },
            success: function(success) {
                if (success == 'true') {
                    renamed = true;
                    console.log("Renamed path succesfully");
                } else {
                    renamed = false;
                    console.log("Could not rename path");
                }
            },
            fail: function(error) {
                console.log(error);
            }
        });
        if (renamed) {
            alert("Renamed path succesfully");
        } else {
            alert("Could not rename path");
        }
    }
}

//Creates an empty GPX file
function createFile(name, creator) {
    name = name.trim().replace("\\", "&bsol;");
    creator = creator.trim().replace("\\", "&bsol;");
    if (name.includes('/')) {
        alert("File name cannot contain '/'");
        return;
    }
    if (name == '') {
        alert("A file name is required");
        return;
    }
    if (!name.endsWith(".gpx")) {
        name += ".gpx";
    }
    if (creator == '') {
        creator = "GPX Creator";
    }
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/createGPX',
        data: {
            fileName: name,
            creator: creator
        },
        success: function(success) {
            if (success) {
                location.reload();
                console.log("File created successfully");
            } else {
                alert("Could not create file");
            }
        },
        fail: function(error) {
            console.log(error);
        }
    });
}

//Adds a text field for a new waypoint
function addPointField() {
    let c = 1;
    while ($(`#lat${c++}`).length != 0);
    c--;
    $(`#lon${c - 1}`).after(`
        <br>
        <label for="lat${c}">Latitude:</label>
        <input type="number" step="any" name="lat${c}" id="lat${c}">
        <label for="lon${c}">Longitude:</label>
        <input type="number" step="any" name="lon${c}" id="lon${c}">
    `);
    console.log("Point field added successfully");
}

//Adds a route to a GPX file
function addRoute(fileName) {
    let waypoints = new Array();
    console.log(fileName);
    if (fileName == '') {
        alert("No file selected");
        return;
    }
    let c = 1;
    while ($(`#lat${c}`).length != 0) {
        let lat = $(`#lat${c}`).val();
        let lon = $(`#lon${c}`).val();
        if (lat != '' && lon != '') {
            if (lat < -90 || lat > 90) {
                alert("Latitude must be between -90 and 90");
                return;
            } else if (lon < -180 || lon > 180) {
                alert("Longitude must be between -180 and 180");
                return;
            }
            waypoints.push($(`#lat${c}`).val(), $(`#lon${c}`).val());
        }
        c++;
    }
    if (waypoints.length / 2 < 2) {
        alert("Route must have at least 2 points");
        return;
    }
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/addRoute',
        data: {
            waypoints: waypoints,
            fileName: fileName
        },
        success: function(valid) {
            if (valid) {
                location.reload();
                console.log("Added route successfully");
            } else {
                console.log("Could not add route");
                alert("Could not add route");
            }
        },
        fail: function(error) {
            console.log(error);
        }
    });
}

//Finds all paths that connect 2 points
function findPaths() {
    let startLat = $('#startLat').val();
    let startLon = $('#startLon').val();
    let endLat = $('#endLat').val();
    let endLon = $('#endLon').val();
    let delta = $('#delta').val();
    if (startLat == '' || startLon == '' || endLat == '' || endLon == '') {
        alert("Start and end points must be defined");
        return;
    } else if (startLat < -90 || startLat > 90 || endLat < -90 || endLat > 90) {
        alert("Latitude must be between -90 and 90");
        return;
    } else if (startLon < -180 || startLon > 180 || endLon < -180 || endLon > 180) {
        alert("Longitude must be between -180 and 180");
        return;
    } else if (delta < 0) {
        alert("Accuracy cannot be negative");
        return;
    }
    $('#foundPathTable').show();
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/findPaths',
        data: {
            startLat: startLat,
            startLon: startLon,
            endLat: endLat,
            endLon: endLon,
            delta: delta
        },
        success: function(pathList) {
            var c = 1;
            $('#foundPathTable').html(
                `
                <tr>
                    <th>Component</th>
                    <th>Name</th>
                    <th>Number of points</th>
                    <th>Length</th>
                    <th>Loop</th>
                </tr>
                `
            );
            if (pathList[0].length == 0 && pathList[1] == 0) {
                $('#foundPathTable').append("<tr><td colspan='5'>No paths found</td></tr>");
            }
            pathList[0].forEach(route => {
                $('#foundPathTable').append(
                    `
                    <tr>
                        <td>Route ${c}</td>
                        <td>${route.name}</td>
                        <td>${route.numPoints}</td>
                        <td>${route.len}m</td>
                        <td>${route.loop}</td>
                    </tr>
                    `
                );
                c++;
            });
            c = 1;
            pathList[1].forEach(track => {
                $('#foundPathTable').append(
                    `
                    <tr>
                        <td>Track ${c}</td>
                        <td>${track.name}</td>
                        <td>${track.numPoints}</td>
                        <td>${track.len}m</td>
                        <td>${track.loop}</td>
                    </tr>
                    `
                );
                c++;
            });
        },
        fail: function(error) {
            console.log(error);
        }
    });
}